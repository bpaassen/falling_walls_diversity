# Falling Walls Diversity
#
# Copyright (C) 2018
# Benjamin Paaßen
# AG Machine Learning
# Bielefeld University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def K_smallest(A, K, taboo=-1):
	"""Find the indices of the K smallest elements in array A.

	Args:
		A (array): An array of numbers or comparables
		K (int): A positive integer, specifying the number of
		smallest elements to be returned
		taboo (int): An index that shall not be included (optional)
	Returns:
		smallest: An array of indices of the K smallest elements.
		If the length of A is shorter than K, all elements of A
		will be returned
	"""

	# If K is larger than the length of the input
	# array, reduce K to the length of the input array
	if(K >= len(A)):
		K = len(A)
		if(taboo >= 0 and taboo < K):
			K -= 1

	smallest = list(range(0, K))
	i_start = K
	if(taboo >= 0 and taboo < K):
		smallest[taboo] = K
		i_start += 1

	# perform insertion sort on the current elements
	for i in range(0, K):
		for j in range(i, 0, -1):
			if(A[smallest[j]] < A[smallest[j-1]]):
				tmp = smallest[j]
				smallest[j] = smallest[j-1]
				smallest[j-1] = tmp
			else:
				break
	# now iterate over the remaining elements and insert
	# into the sorted list
	for i in range(i_start, len(A)):
		if(i == taboo):
			continue

		if(A[i] < A[smallest[-1]]):
			j = K-1
			while(j > 0 and A[smallest[j-1]] > A[i]):
				smallest[j] = smallest[j-1]
				j-=1
			smallest[j] = i
	return smallest

def Knn(d, y, K, taboo=-1):
	"""Perform a k-nearest neighbor classification.

	Args:
		d (array): An array of distances of the point to be
		classified to all other data points.
		y (array): An array specifying the labels for all data points.
		K (int): The number of nearest neighbors to consider
		taboo (int): An index that shall not be considered for
		the search (optional)
	Returns:
		y: The predicted label for the data point
	"""
	# check for the closest neighbors
	smallest = K_smallest(d, K, taboo)
	# build a histogram of labels
	hist = {}
	for i in smallest:
		if(y[i] in hist):
			 hist[y[i]] += 1
		else:
			hist[y[i]] = 1
	# check for the majority label
	maj_y = -1
	max_votes = 0
	for y in hist:
		if(hist[y] > max_votes):
			maj_y = y
			max_votes = hist[y]
	return maj_y
