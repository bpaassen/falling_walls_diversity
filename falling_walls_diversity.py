# Falling Walls Diversity
#
# Copyright (C) 2018
# Benjamin Paaßen
# AG Machine Learning
# Bielefeld University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import csv
import math
import numpy as np
import re
import sys
from scipy.sparse import csr_matrix
from scipy.optimize import linprog
from knn import K_smallest
from knn import Knn

# Print help if the user tries to provide command line arguments

print()

def print_help():
	print("""Falling Walls Diversity
Copyright (C) 2018 - Benjamin Paaßen

This tool is intended to distribute the participants of the Falling Walls
Circle 2018 into maximally diverse groups. This algorithm requires at least two
files to work. First, a CSV file with the following columns:

1. Age - either a positive integer or an age range denoted with a dash, e.g. 41-50
2. Gender - any string specifying the self-identified gender
3. Nationality - a country name
4. Academic Degree - either None, Bachelor, Master, Doctor, or Professor
5. Distance to Berlin - a non-negative integer
6. (Number of) Google Search Results - a non-negative integer
7. AI Optimistic / Pessimistic - any string (typically 'yes' or 'no')
8. Expert/Non-Expert - any string (typically 'yes' or 'no')
9. Returning Circle Guest - any string (typically 'yes' or 'no')
10. Falling Walls Speaker - any string (typically 'yes' or 'no')
11. Breakthrough - any string (typically 'technological' or 'societal')
12. Doubt - any string (typically 'technological' or 'societal')

Second, a list of CSV files with the following columns

1. participant index
2. group index.

The tool first weighs the ten attributes of the first CSV file
to best explain the groupings in the remaining CSV files. Then, the tool
uses the resulting distance measure to construct R work groups which are
as diverse as possible and prints them to the command line.

Command line usage:

python3 falling_walls_diversity.py <R> [-J <J>] <participants.csv> <groupings1.csv> [groupings2.csv] ... [groupingsN.csv]

where
<R> is the number of work groups which should be constructed.
<J> (optional) is the number of nearest neighbors to consider for
  weight learning. 1 per default.
<participants.csv> is the CSV file listing the participants as rows.
<groupings...csv> is a CSV file with two columns, where the first column
  is a participant index and the second column is a group index; each file
  has to define at least two groups with at least one member each.

Please refer to the README.md for further information.""")

# Prepare the command line arguments
if(len(sys.argv) > 3):
	# First, the number of work groups
	if(sys.argv[1].isdigit()):
		no_groups = int(sys.argv[1])
		if(no_groups < 2):
			raise ValueError('The number of work groups needs to be at least 2.')
	else:
		print_help()
		sys.exit()
	argv_idx = 2
	if(sys.argv[2] == '-J'):
		if(sys.argv[3].isdigit()):
			no_neighbors = int(sys.argv[3])
			if(no_neighbors < 1):
				raise ValueError('The number of neighbors needs to be at least 2.')
		else:
			raise ValueError('Expected an integer value after -J')
		argv_idx += 2
	else:
		no_neighbors = 1
	# Then the participants csv file
	participants_file = sys.argv[argv_idx]
	# Then the groupings files
	grouping_files = sys.argv[argv_idx+1:]
else:
	print_help()
	sys.exit()

# Read the participant table data
# After this section, X will be a list, where each entry
# is a row of our table. The columns contain the data about
# the participants.
X = []

K = 12

with open(participants_file, newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
	# read the first row to obtain the headers
	headers = next(reader)
	if(len(headers) != K):
		raise ValueError('The number of columns in the participants CSV file needs to be {}.'.format(K))
	# read the participant attributes
	for row in reader:
		X.append(row)

## STEP 1: Compute the distances between all participants for all
# ten attributes

m  = len(X)
print('Successfully read data for {} participants.\n'.format(m))
Ds = np.zeros((K, m, m))

if(no_groups * 2 > m):
	raise ValueError('You have asked for {} work groups, but we need at least 2 participants per group and there are only {} participants overall.'.format(no_groups, m))

# Age (column 0)
k = 0

age_regex = re.compile('(\d*)(\-(\d*))?')

ages = np.zeros(m)
for i in range(0, m):
	match = age_regex.match(X[i][k])
	if(not match):
		raise ValueError('Expected an age in the first column, denoted as a single integer or as two integers delimited by a dash, but got {}'.format(X[i][k]))
	if(not match.group(3)):
		ages[i] = float(match.group(1))
	else:
		ages[i] = 0.5 * (float(match.group(1)) + float(match.group(3)))

age_Z = np.max(ages) - np.min(ages)

for i in range(0, m):
	for j in range(i+1, m):
		Ds[k, i, j] = np.abs(ages[i] - ages[j]) / age_Z
		Ds[k, j, i] = Ds[k, i, j]

# Gender (column 1)
k = k+1

for i in range(0, m):
	for j in range(i+1, m):
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Nationality (column 2)
k = k+1

for i in range(0, m):
	for j in range(i+1, m):
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Academic Degree (column 3)
k = k+1

degrees = np.zeros(m)
for i in range(0, m):

	if(X[i][k] == 'None' or X[i][k] == '' or X[i][k] == 'non'):
		degrees[i] = 0
	elif(X[i][k] == 'Bachelor' or X[i][k] == 'B.A.' or X[i][k] == 'B.Sc.'):
		degrees[i] = 1
	elif(X[i][k] == 'Master' or X[i][k] == 'M.A.' or X[i][k] == 'M.Sc.'):
		degrees[i] = 2
	elif(X[i][k] == 'Doctor' or X[i][k] == 'Dr' or X[i][k] == 'Dr.' or X[i][k] == 'PhD'):
		degrees[i] = 3
	elif(X[i][k] == 'Professor' or X[i][k] == 'Prof' or X[i][k] == 'Prof.'):
		degrees[i] = 4
	else:
		raise ValueError('Expected an academic degree in the fourth column, either None, Bachelor, Master, Doctor, or Professor, but got {}'.format(X[i][k]))

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		Ds[k, i, j] = np.abs(degrees[i] - degrees[j]) / 4
		Ds[k, j, i] = Ds[k, i, j]

# distance to Berlin (column 4)
k = k+1

dist_berlin = np.zeros(m)
for i in range(0, m):
	if(X[i][k] == '' or int(X[i][k]) == 0):
		dist_berlin[i] = 0
	else:
		dist_berlin[i] = np.log10(float(X[i][k]))

dist_berlin_Z = np.max(dist_berlin) - np.min(dist_berlin)

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		Ds[k, i, j] = np.abs(dist_berlin[i] - dist_berlin[j]) / dist_berlin_Z
		Ds[k, j, i] = Ds[k, i, j]

# no. of google search results (column 5)
k = k+1

google_results = np.zeros(m)
for i in range(0, m):
	if(X[i][k] == '' or int(X[i][k]) == 0):
		google_results[i] = 0
	else:
		google_results[i] = np.log10(float(X[i][k]))

google_results_Z = np.max(google_results) - np.min(google_results)

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		Ds[k, i, j] = np.abs(google_results[i] - google_results[j]) / google_results_Z
		Ds[k, j, i] = Ds[k, i, j]

# AI Optimism (column 6)
k = k+1

ai_optimism = np.zeros(m)
for i in range(0, m):
	if(X[i][k] == ''):
		ai_optimism[i] = 0
	else:
		ai_optimism[i] = int(X[i][k])

ai_optimism_Z = np.max(ai_optimism) - np.min(ai_optimism)

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		Ds[k, i, j] = np.abs(ai_optimism[i] - ai_optimism[j]) / ai_optimism_Z
		Ds[k, j, i] = Ds[k, i, j]

# Expert (column 7)
k = k+1

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Returning Circle Guest (column 8)
k = k+1

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Falling Walls Speaker (column 9)
k = k+1

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Breakthrough (column 10)
k = k+1

for i in range(0, m):
	for j in range(i+1, m):
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

# Doubt (column 11)
k = k+1

for i in range(0, m):
	if(X[i][k] == ''):
		continue
	for j in range(i+1, m):
		if(X[j][k] == ''):
			continue
		if(X[i][k] != X[j][k]):
			Ds[k, i, j] = 1
		Ds[k, j, i] = Ds[k, i, j]

k = k+1

if(k != K):
	raise ValueError('Internal error: Read less columns then necessary!')

## STEP 2: Learn the correct weighting of the attributes
# via large margin nearest neighbor (LMNN)
# based on the pre-defined groupings

# We learn the weighting alpha via a linear program.
# The original form of the linear program is:
# minimize w.r.t. alpha, such that the sum over all pairwise
# distances within the same group is as small as possible
# and all participants from a different group are at
# least 1 unit further away

# In slack-variable form this program is re-written to
# the following:
# minimize w.r.t. alpha and xi the sum over all i,
# the closest j with y[i] = y[j] and all l with y[l] != y[i] over
# xi[i, j, l]
# plus the sum over all k, all i, and the closest j with y[i] = y[j]
# of alpha[k] * d[k, i, j]
# under the side constraints alpha[k] >= 0, xi[i, j, l] >= 0,
# and sum over k alpha[k] * (d[k, i, j] - d[k, i, l]) - xi[i, j, l]
# <= -1

# If we re-write this into standard form, we obtain
# the problem [alpha, xi] * z, s.t. [alpha, xi] * A <= b
# with
#
# K = 12
#
# z[t] = sum over i, the closest j with y[i] = y[j] :
#     d[t, i, j] for t < K
# z[t] = 1 for t >= K
#
# A[s, t] = d[t, i, j] - d[t, i, l] if s = t(i, j, l), t < K
# A[s, t] = -1 if s = t(i, j, l), t = K + t(i, j, l)
# A[s, t] = -1 if s = t + T
# A[s, t] = 0 otherwise
#
# b[s] = -1 for s < T
# b[s] = 0 otherwise
#
# where t(i, j, l) is a function which assigns a linear index
# to all triples (i, j, l) where j is the closest data point to i
# with y[j] = y[i] and where y[l] != y[i].
# and T is the overall number of such triples.
#
# Note that we require slack variables xi for every
# grouping separately and that the number of slack variables
# scales quadratically with the number of people considered in
# any grouping. This makes this linear program relatively
# inefficient. Yet, because we consider a sufficiently small
# problem, it is still feasible.

# First, set up some helper variables to construct the overall
# linear program later on

# The objective function coefficients with a small
# regularization coefficient to ensure that the weights
# stay in a reasonable range.
# Negative values encourage large weights, positive
# values encourage small weights. This is equivalent
# to an L1 regularization.
regul = 1E-3;
z = np.ones(K) * regul;
# The list or row indices, column indices, and
# values for the constraint matrix A
rows = []
cols = []
vals = []
# The constant vector b
b = []
# The overall number of slack variables T
T = 0

# Then, iterate over all groupings
Y = []
I = []
for grouping_file in grouping_files:

	# First, read the table with group assignments
	idxs = []
	ys = []
	group_idxs = {}
	C = 0

	with open(grouping_file, newline='') as csvfile:
		print('Reading grouping {}\n'.format(grouping_file))
		reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
		# read the first row to obtain the headers
		header_row = next(reader)
		# read the participant attributes
		for row in reader:
			idxs.append(int(row[0])-1)
			y = int(row[1])
			ys.append(y)
			if(y not in group_idxs):
				group_idxs[y] = C
				C += 1

	I.append(idxs)
	Y.append(ys)

	n = len(idxs)

	print('Successfully read grouping of {} participants.\n'.format(n))

	groups = []

	# reformat to a group listing
	for c in range(0, C):
		groups.append([])

	for i in range(0, n):
		groups[group_idxs[ys[i]]].append(idxs[i])

	# prepare the mapping of t to triples (i, j, l)
	triples = []
	for c in range(0, C):
		for i in range(0, len(groups[c])):
			# Compute the distances between i and
			# all other points in the groups
			ds = np.zeros(len(groups[c]))
			for j in range(0, len(groups[c])):
				for k in range(0, K):
					ds[j] += Ds[k, groups[c][i], groups[c][j]]

			# Identify the no_neighbors closest neighbors
			# to i in the group.
			neighbors = K_smallest(ds, no_neighbors, i)

			# Now, iterate over those neighbors
			for j in neighbors:
				# add the distance to the closest neighbors to the
				# objective function
				for k in range(0, K):
					z[k] += Ds[k, groups[c][i], groups[c][j]]
				# generate all triples (i, j, l) where y[i] != y[l]
				for c2 in range(0, C):
					if(c == c2):
						continue
					for l in groups[c2]:
						triples.append([groups[c][i], groups[c][j], l])

	# Then set up the constaint matrix coefficients
	# for the current grouping

	# The slack variable constaints, i.e.
	# sum over k alpha[k] * (d[k, i, j] - d[k, i, l])
	# - xi[i, j, l] <= -1
	for t in range(0, len(triples)):
		i = triples[t][0]
		j = triples[t][1]
		l = triples[t][2]
		for k in range(0, K):
			rows.append(T + t)
			cols.append(k)
			vals.append(Ds[k, i, j] - Ds[k, i, l])
		rows.append(T + t)
		cols.append(K + T + t)
		vals.append(-1)
		b.append(-1)

	T += len(triples)

# After we have read all groupings, finalize the linear program.

# First, we need a coefficient of 1 for all slack variables
# in the objective function, because we wish to minimize over the
# slack variables
z = np.append(z, np.ones(T));

## Additionally, we enforce that each weight is at least a beta fraction
## of the overall weights

enforce_weights = False
beta = 0.03

if(enforce_weights):
	for k in range(0, K):
		rows.append(T+k)
		cols.append(k)
		vals.append(beta-1)
		b.append(0)
		for l in range(0, K):
			if(l == k):
				continue
			rows.append(T+k)
			cols.append(l)
			vals.append(beta)
	# Set up the overall constraint matrix in sparse form
	A = csr_matrix((vals, (rows, cols)), shape=(T+K, K+T))
else:
	A = csr_matrix((vals, (rows, cols)), shape=(T, K+T))

# Finally, transform b into a numpy array
b = np.array(b)

# Solve the actual linear problem
# Important! We explicitly put in non-negativity bounds here!
print('Learning optimal weights now via a linear program with {} variables and {} constraints (this may take a while) ...\n'.format(K+T, np.max(rows)+1));

sol = linprog(z, A, b, bounds=(0, None), method='interior-point', options={'sparse': True})

print("Finished learning. The LMNN loss is {}.\n".format(sol.fun))

# Retrieve the weights alpha from the solution

alpha = np.zeros(K)
for k in range(0, K):
	alpha[k] = sol.x[k]

# Renormalize the weights to sum to 1
alpha = np.divide(alpha, np.sum(alpha))
# Cut small weights and renormalize again
alpha[alpha < 1E-6] = 0
alpha = np.divide(alpha, np.sum(alpha))

# Print the weights
print("The final weights are:\n")
for k in range(0, K):
	print('{} : {}%'.format(headers[k], int(round(alpha[k]*100))))


# Compute the final pairwise distance matrix

D = np.zeros((m, m))

for i in range(0, m):
	for j in range(i+1, m):
		for k in range(0, K):
			D[i, j] += alpha[k] * Ds[k, i, j]
		D[j, i] = D[i, j]

# Perform a k-NN classification, thus checking how well the found weighting
# explains the groupings
for f in range(0, len(Y)):
	err = 0
	for i in range(0, len(I[f])):
		y_i_pred = Knn(D[I[f][i],I[f]], Y[f], no_neighbors, i)
		if(y_i_pred != Y[f][i]):
			err +=1
	print('\nclassification error for {}th grouping is {}%'.format(f+1, 100 * err / m))

print()

## STEP 3: Generate work groups

# Now that the pairwise distances are computed, we can start with assembling
# the actual work groups.

# initialize the set of remaining participants
Phi = set(range(no_groups, m))

# initialize the work groups with the first no_groups participants
# as chairs

group_divs = np.zeros(no_groups)
groups = []

for r in range(0, no_groups):
	groups.append([r])

# Start the greedy max diversity algorithm. We iterate until all participants are assigned
for t in range(0, m-no_groups):
	# select the least diverse group at this point
	r = np.argmin(group_divs)
	# select the remaining participant who would increase the diversity in the rth
	# group the most
	j_max = -1
	d_max = -1
	for j in Phi:
		# compute the distance of j to all participants in the rth group.
		# This would be the diversity j adds to group r.
		d = 2 * np.sum(D[j, groups[r]])
		# check if it is better than the current diversity advantage
		if(d > d_max):
			d_max = d
			j_max = j
	# remove the selected point from Phi and add it to the rth group
	Phi.remove(j_max)
	groups[r].append(j_max)
	group_divs[r] += d_max

# Print out the resulting groups
print('Finished generating work groups.\n')
for r in range(0, no_groups):
	groups[r].sort()
	print('group {} with average diversity = {}'.format(r+1, group_divs[r] / (len(groups[r]) * len(groups[r]))))
	print(np.array(groups[r])+1)
