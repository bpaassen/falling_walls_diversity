# Falling Walls Diversity

Copyright (C) 2018  
Benjamin Paaßen  
AG Machine Learning  
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Introduction

In a communicative and open environment, diverse perspectives in a work group
can enhance group functioning, as well as group member satisfaction, intent to
remain, commitment, perceived work group performance, and visibility of
minority groups ([Jehn et al., 1999][1], [Van Knippenberg & Schippers, 2007][2],
[Karimi et al., 2017][3]). As such, conferences where participants voluntarily
engage in open interaction appear like the ideal setting to yield the benefits
of work group diversity.

This software contains a machine learning approach to distribute participants
of the [Falling Walls Circle](https://www.falling-walls.com/circle/about)
into maximum diversity work groups. For this purpose, the program takes human
input to infer how diversity should be quantified and then distributes the
participants accordingly. For more details, please refer to the 'Algorithm'
section below. This project is an extension of the IK 2018 contribution
'Greedy for Diversity' by Franz and Paaßen (2018). Special thanks go to
Lio Zam Franz for the original idea and the initial project, and to
Weronika Perlinski as well as Matyas Kovacs for their support in this
implementation.

## Quickstart Guide

To use the program, you require [Python 3](https://www.python.org/downloads/),
[scipy version 1.1 or higher](https://scipy.org/install.html) and the following
files in this directory.

1. `participants.csv`: A table listing the diversity-relevant attributes for
all participants. Participants are listes as rows. The program expects the
following columns:
    1. Age - either a positive integer or an age range denoted with a dash, e.g. 41-50
    2. Gender - any string specifying the self-identified gender
    3. Nationality - a country name
    4. Academic Degree - either None, Bachelor, Master, Doctor, or Professor
    5. Distance to Berlin - a non-negative integer
    6. (Number of) Google Search Results - a non-negative integer
    7. AI Optimistic / Pessimistic - any string (typically 'yes' or 'no')
    8. Expert/Non-Expert - any string (typically 'yes' or 'no')
    9. Returning Circle Guest - any string (typically 'yes' or 'no')
    10. Falling Walls Speaker - any string (typically 'yes' or 'no')
    11. Breakthrough - any string (typically 'technological' or 'societal')
    12. Doubt - any string (typically 'technological' or 'societal')
2. `groups.csv`: A table which distributes a subset of the participants into
groups, such that the groups are _as homogenous as possible_ according to your
intuition. The first column of the table should contain indices of participants
and the second column of the table should contain group indices. For example,
the row "1,2" would signify that the first participant in the `participants.csv`
table is assigned to the second group.

Example versions of these files are available in this directory.
Once you have constructed these files, you can generate work groups by
executing the following command in your command line:

<pre>python3 falling_walls_diversity.py &lt;R&gt; participants.csv groups.csv</pre>

where &lt;R&gt; is the number of work groups you would like to generate.
The work groups will be printed on the command line in the form of
indices.

## Algorithm

The algorithm has three steps. 
First, we quantify diversity along each attribute.
Second, we infer how important each attribute is to explain the grouping
given in the `groups.csv` file. This yields our overall measure of distance
between participants.
Third, we distribute people into work groups such as to maximize the pairwise
distance between all work group members.

We describe each of these steps now in turn.

### Quantifying distance

For each of the attributes, we define a specific measure of distance between
participants. In particular, let $`x_{i,k}`$ denote the value of attribute
k for participant i and let $`x_{j,k}`$ denote the value of attribute k
for participant j. Then, we define:

1. Age: We consider the absolute difference
$`|x_{i, 1} - x_{j, 1}|`$ between the age $`x_{i, 1}`$ of
participant i and the age $`x_{j, 1}`$ of participant j and divide
it by the difference between the age of the oldest participant and the age of
the youngest participant.
2. Gender: We define the distance between different genders as 1 and between
same genders as 0.
3. Nationality: We define the distance between different nationalities as 1 and
between same nationalities as 0.
4. Academic Degree: To each degree on the scale None, Bachelor, Master, Doctor, and
Professor we assign the numbers zero to four and we take the absolute difference
$`|x_{i, 4} - x_{j, 4}|`$ between the degree number $`x_{i, 4}`$
of participant i and the degree number $`x_{j, 4}`$ of participant j
and divide that by 4.
5. Distance to Berlin: We consider the absolute difference
$`|\log(x_{i, 5}) - \log(x_{j, 5})|`$ between the logarithm of the
distance to berlin $`x_{i, 5}`$ of participant i and the
logarithm of the distance to berlin $`x_{j, 5}`$ of
participant j and divide it by the maximum log-difference in the data set.
6. Google Search Results: We consider the absolute difference
$`|\log(x_{i, 6}) - \log(x_{j, 6})|`$ between the logarithm of the
number of google search results $`x_{i, 6}`$ of participant i and the
logarithm of the number of google search results $`x_{j, 6}`$ of
participant j and divide it by the maximum log-difference in the data set.
7. AI Optimistic/Pessimistic: We define the distance between equal entries as
zero and between unequal entries as one.
8. Expert/Non-Expert: We define the distance between equal entries as
zero and between unequal entries as one.
9. Returning Circle Guest: We define the distance between equal entries as
zero and between unequal entries as one.
10. Falling Walls Speaker: We define the distance between equal entries as
zero and between unequal entries as one.
11. Breakthrough: We define the distance between equal entries as zero and
between unequal entries as one.
11. Doubt: We define the distance between equal entries as zero and
between unequal entries as one.

In all cases, we regard the distance to missing data as zero.
For each attribute k we denote the resulting distance between participant i
and participant j as $`d^k_{i,j}`$

### Learning Attribute Importance

Now that we have defined the distance for each attribute, the question arises
how we combine the distances across different attributes to quantify the overall
distance $`d_{i,j}`$ between two participants i and j. Our approach is to
perform a weighted average of the attribute-specific distances. More precisely,
we define $`d_{i,j}`$ as follows.

```math
d_{i, j} := \sum_{k=1}^{10} \alpha_k \cdot d^k_{i, j}
```

where $`\alpha_k`$ is a non-negative numeric weight for attribute k,
which quantifies how important attribute k is to our intuition of diversity.

For our algorithm we assume that humans can not directly specify the numeric
weights $`\alpha_k`$, but that they have some intuition regarding
what constitutes a homogenous group. In particular, we assume that they
can partition a subset of the participant into disjoint groups, such that
each of the group is intuitively homogenous but the groups are distinct from
each other. This grouping is the content of the `groups.csv` file.

Our aim is now to find weights $`\alpha_k`$ such that the distance
$`d_{i,j}`$ between participants i and j from the same group is as low
as possible and the distance $`d_{i, l}`$ between participants i and l
from different groups is at least as high as the largest distance to a
member of the same group. This is is essentially the same as
_large margin nearest neighbor metric learning_ (LMNN) as described by
[Weinberger and Saul (2009)][4]. Analogous to their approach, this algorithm
infers the weights $`\alpha_k`$ by solving the following minimization
problem:

```math
\min_{\vec \alpha} \sum_i \sum_{j \in N_J(i)} d_{i, j} +
\sum_{l \in I(i, j)} d_{i, j} + 1 - d_{i, l}
```

where $`N_J(i)`$ denotes the set of the J closest neighbors to i
in the same group and $`I(i, j)`$ denotes the set of all points which are in a
different group than i but which are closer to i compared to j plus a margin
of one, that is:

```math
I(i, j) := \{ l | y_i \neq y_l \wedge d_{i, l} \leq d_{i, j} + 1 \}
```

The number of closest neighbors J is a hyper-parameter of the model which
can be set on the command line via a `-J` option. It is five per default.

Note that this minimization problem is a linear program if we take the
set $`N_J(i)`$ to be a constant. We solve this problem using the
interior point solver of scipy.

### The Maximum Diversity Partition Problem

Now that we have obtained our final distance measure $`d_{i,j}`$ between
participants, we can distribute the participants into diverse work groups.
In formal terms, a work group $`I_r`$ is a set of participants, and we
quantify the diversity $`\Delta(I_r)`$ within the work group as the sum
over all pairwise distances, that is:

```math
\Delta(I_r) := \sum_{i \in I_r} \sum_{j \in I_r} d_{i, j}
```

Our overall problem to solve is now to partition all participants into R
disjoint work groups, such that the least diverse work group is as diverse
as possible, that is:

```math
\max_{I_1, \ldots, I_R \text{ disjoint}} \min_r \Delta(I_r)
```

Unfortunately, it provably infeasible to find an exact solution for this problem
(refer to Franz and Paaßen, 2018 for an NP-hardness proof). Still, there exist
a fair amount of heuristics for a good solution (refer e.g. to
[Marti et al., 2013][5]). In this case, we apply a greedy heuristic inspired
by [Erkut (1990)][6], which works as follows:

1. Initialize all work groups $`I_1, \ldots, I_R`$ as empty sets.
2. Select $`I_r`$ as the work group with the lowest diversity
$`\Delta(I_r)`$ at the moment.
    a. If $`I_r`$ is empty, select the two participants i and j which are
    not yet member of any group and have the highest distance $`d_{i, j}`$.
    Assign both of them to group $`I_r`$.
    b. If $`I_r`$ is _not_ empty, select the participant i which would
    increase the diversity of group $`I_r`$ the most.
    Assign this participant to group $`I_r`$.
5. If there are no participants left to assign, end the algorithm. Otherwise
return to step 2.

### Licenses and Re-Use

This documentation as well as the example files `participants.csv` and `groups.csv`
are licensed under the terms of the
[creative commons attribution-shareAlike 4.0 international (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license.
The code contained alongside this documentation is licensed under the
[GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) license.
A copy of this license is contained in the `LICENSE` file alongside this README.

We link against SciPy according to the [SciPy license](https://www.scipy.org/scipylib/license.html).
We link against NumPy according to the [NumPy license](http://www.numpy.org/license.html).

## References

* Franz, L. and Paaßen, B. (2018). Greedy for Diversity: The Participant
Partition Problem in IK Interaction Groups. Poster at the Interdisciplinary
College 2018. (available as `franz_paassen_maximum_diversity.pdf` in this GIT directory)
* Jehn, K., Northcraft, G., and Neale, M. (1999). Why Differences Make a
Difference: A Field Study of Diversity, Conflict and Performance in Workgroups.
Administrative Science Quarterly, 44(4), 741-763. doi:[10.2307/2667054][1]
* Van Knippenberg, D., and Schippers, M. (2007). Work Group Diversity.
Annual Review of Psychology, 58, 515-541. doi:[10.1146/annurev.psych.58.110405.085546][2]
* Karimi, F., Genois, M., Wagner, C., Singer, P., and Strohmaier, M. (2017).
Visibility of minorities in social networks. arXiv:[1702.00150][3].
* Weinberger, K., and Saul, L. (2009).  Distance Metric Learning for Large
Margin Nearest Neighbor Classification. Journal of Machine Learning Research,
10, 207-244. [Link][4]
* Marti, R., Gallego, M., Duarte, A., and Pardo, E. (2013). Heuristics and
metaheuristics for the maximum diversity problem. Journal of Heuristics, 19(4),
591-615. doi:[10.1007/s10732-011-9172-4][5]
* Erkut, E. (1990). The discrete p-dispersion problem. European Journal of
Operational Research, 46(1), 48-60. doi:[10.1016/0377-2217(90)90297-O][6]

[1]:https://doi.org/10.2307/2667054 "Jehn, K., Northcraft, G., and Neale, M. (1999). Why Differences Make a Difference: A Field Study of Diversity, Conflict and Performance in Workgroups. Administrative Science Quarterly, 44(4), 741-763. doi:10.2307/2667054"
[2]:https://doi.org/10.1146/annurev.psych.58.110405.085546 "Van Knippenberg, D., and Schippers, M. (2007). Work Group Diversity. Annual Review of Psychology, 58, 515-541. doi:10.1146/annurev.psych.58.110405.085546"
[3]:https://arxiv.org/abs/1702.00150 "Karimi, F., Genois, M., Wagner, C., Singer, P., and Strohmaier, M. (2017). Visibility of minorities in social networks. arXiv:1702.00150."
[4]:http://www.jmlr.org/papers/v10/weinberger09a.html "Weinberger, K., and Saul, L. (2009).  Distance Metric Learning for Large Margin Nearest Neighbor Classification. Journal of Machine Learning Research, 10, 207-244."
[5]:https://doi.org/10.1007/s10732-011-9172-4 "Marti, R., Gallego, M., Duarte, A., and Pardo, E. (2013). Heuristics and metaheuristics for the maximum diversity problem. Journal of Heuristics, 19(4), 591-615. doi:10.1007/s10732-011-9172-4"
[6]:https://doi.org/10.1016/0377-2217(90)90297-O "Erkut, E. (1990). The discrete p-dispersion problem. European Journal of Operational Research, 46(1), 48-60. doi:10.1016/0377-2217(90)90297-O"
